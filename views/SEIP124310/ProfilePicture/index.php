<?php
session_start();
include_once('../../../vendor/autoload.php.');
use App\Bitm\SEIP124310\ProfilePicture\ImageUploader;
use App\Bitm\SEIP124310\Utility\Utility;
use App\Bitm\SEIP124310\Message\Message;


$profilePicture= new ImageUploader();
$allInfo=$profilePicture->index();
//Utility::d($allBook);
?>

<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../Resources/bootstrap/css/bootstrap.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <!--  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>-->
</head>
<body>

<div class="container">
    <h2>User info List</h2>

    <a href="create.php" class="btn btn-info" role="button">Add User info</a> <br><br>
    <br>
    <div id="message">

        <?php if((array_key_exists('message',$_SESSION)&& !empty($_SESSION['message']))){
            echo Message::message();
        }?>
    </div>
    <div class="panel panel-primary">
        <div class="panel-body">
            <?php
            $_allInfo1= array();
            $query1="SELECT * FROM `profilepicture` WHERE `profilepicture`.`deleted_at` = 'A' ";
            $conn = mysqli_connect("localhost", "root", "", "atomicprojectb21") or die("Database connection failed");
            $result2= mysqli_query($conn,$query1);
            //You can also use mysqli_fetch_object e.g: $row= mysqli_fetch_object($result)
            while($row= mysqli_fetch_assoc($result2)){
                $_allInfo1[]=$row;
                //print_r($_allInfo1['0']['name']);

            ?>
            <img src="../../../Resources/Images/<?php echo $_allInfo1['0']['images']?>" class="img-responsive" alt="Cinque Terre" width="304" height="236">
            <?php } ?>
        </div>
    </div>
    <div class="table-responsive">
        <table class="table">
            <thead>
            <tr>
                <th>SL#</th>
                <th>ID</th>
                <th>Name</th>
                <th>Profile Picture</th>
                <th>Action</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $sl=0;
            foreach($allInfo as $info){
                $sl++;
                ?>
                <tr>
                    <td><?php echo $sl; ?></td>
                    <td><?php echo $info['id'] // for object: $book->id ; ?></td>
                    <td><?php echo $info['name'] // for object: $book->title; ?></td>
                    <td><img src="../../../Resources/Images/<?php echo $info['images']?>" alt="image" height="100px" width="100px"></td>
                    <td>
                        <a href="active.php?id=<?php echo $info['id']?>" class="btn btn-info  btn-xs" role="button">Active</a>
                        <a href="inactive.php?id=<?php echo $info['id']?>" class="btn btn-danger  btn-xs" role="button">Inactive</a>
                        <a href="view.php?id=<?php echo $info['id']?>" class="btn btn-info  btn-xs" role="button">View</a>
                        <a href="edit.php?id=<?php echo $info['id']?>" class="btn btn-primary  btn-xs" role="button">Edit</a>
                        <a href="delete.php?id=<?php echo$info['id']?>" class="btn btn-danger  btn-xs" role="button">Delete</a>
                    </td>


                </tr>
            <?php } ?>


            </tbody>
        </table>
    </div>
</div>
<script>
    $('#message').show().delay(3000).fadeOut();
</script>

</body>
</html>
